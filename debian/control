Source: sugar
Section: x11
Priority: optional
Maintainer: Debian Sugar Team <pkg-sugar-devel@lists.alioth.debian.org>
Uploaders: Jonas Smedegaard <dr@jones.dk>,
 Santiago Ruano Rincón <santiago@debian.org>
Build-Depends: cdbs,
 python,
 python-dev (>= 2.3.5-7),
 debhelper,
 dh-buildinfo,
 libgtk-3-dev,
 libglib2.0-dev,
 python-gtk2-dev,
 libgconf2-dev,
 gettext,
 intltool,
 shared-mime-info,
 python-empy
Standards-Version: 4.1.3
Vcs-Git: https://anonscm.debian.org/git/pkg-sugar/sugar.git
Vcs-Browser: https://anonscm.debian.org/git/pkg-sugar/sugar.git
Homepage: http://wiki.sugarlabs.org/go/Sugar

Package: sucrose
Section: metapackages
Architecture: all
Depends: ${cdbs:Depends},
 ${misc:Depends}
Recommends: ${cdbs:Recommends}
Suggests: ${cdbs:Suggests}
Provides: ${cdbs:Provides},
 ${python:Provides}
Conflicts: ${cdbs:Conflicts}
Replaces: ${cdbs:Replaces}
Description: Sugar Learning Platform - Sucrose
 Sugar Learning Platform promotes collaborative learning through Sugar
 Activities that encourage critical thinking, the heart of a quality
 education.  Designed from the ground up especially for children, Sugar
 offers an alternative to traditional “office-desktop” software.
 .
 This package depends on all packages which make up Sucrose, the
 official base Sugar environment.

Package: sugar-session
Architecture: all
Depends: ${cdbs:Depends},
 ${misc:Depends},
 ${python:Depends},
 ${shlibs:Depends}
Recommends: ${cdbs:Recommends}
Suggests: ${cdbs:Suggests}
Provides: ${cdbs:Provides},
 ${python:Provides}
Breaks: ${cdbs:Breaks}
Conflicts: ${cdbs:Conflicts}
Replaces: ${cdbs:Replaces}
Description: Sugar Learning Platform - window manager
 Sugar Learning Platform promotes collaborative learning through Sugar
 Activities that encourage critical thinking, the heart of a quality
 education.  Designed from the ground up especially for children, Sugar
 offers an alternative to traditional “office-desktop” software.
 .
 This package contains the integrated session and window manager Sugar.

Package: python-jarabe
Section: python
Architecture: all
Depends: ${cdbs:Depends},
 ${misc:Depends},
 ${python:Depends},
 ${shlibs:Depends}
Recommends: ${cdbs:Recommends}
Suggests: ${cdbs:Suggests}
Provides: ${cdbs:Provides},
 ${python:Provides}
Breaks: ${cdbs:Breaks}
Conflicts: ${cdbs:Conflicts}
Replaces: ${cdbs:Replaces}
Description: Sugar Learning Platform - graphical shell
 Sugar Learning Platform promotes collaborative learning through Sugar
 Activities that encourage critical thinking, the heart of a quality
 education.  Designed from the ground up especially for children, Sugar
 offers an alternative to traditional “office-desktop” software.
 .
 "Jarabe" is Spanish for "Jelly" - a sugary layer spread on the surface.
 .
 This package contains the parts of Sugar Learning Platform immediately
 visible to the user - the views, the journal, and the control panel.
 .
 This is a graphical shell, the name does not refer to a command-line
 "shell" interface.
